# Welcome to the Radio Info Page!

```
last updated (2023-10-13 11:00pm "New Beginnings")

Added (4 changes)

Created RADIO.md file itself
-Created Getting Started section
--Added short tutorial for people who have never used the internet before

-Created Future Plans section
--Added summary of the idea of a possible endpoint for the radio in the far future
--Created Plans/ToDo List subsection
---Added 1 plan for the future 
```

## Getting started

To start listening to the radio, simply hover over the box titled 'Songs' and click on one of the names in the list.

## Future plans 

I want to make the radio into a system of people putting songs on immediently or in the queue to play later.  
Obviously it is nowhere near that point so I'll just go from here and see where I land.

### Plans/ToDo List

- Make songs automatically start playing when the previous one ends.
